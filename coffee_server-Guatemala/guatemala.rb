# coding: utf-8
require "rubygems"
require "sinatra"
require "erb"
require "rubytter"
require "oauth"
require "systemu"
require "rbconfig"
require_relative "models/coffee_ftdi"
require_relative "models/user"
require_relative "models/coffee_cup"
require_relative "models/config"

configure do
  use Rack::Session::Cookie
  Mongoid.raise_not_found_error = false
  set :logging, true
  set :dump_errors, true
  set :show_exceptions, true
  AppConfig[:COFFEE_ADDRESS] = ENV['COFFEE_ADDRESS'] unless ENV['COFFEE_ADDRESS'].nil?
end

before '/signed/*' do
  if session[:user_id] == nil
    redirect '/'
  end

  @server_twitter = User.new
  @server_twitter.access_token = AppConfig[:ACCESS_TOKEN]
  @server_twitter.access_secret = AppConfig[:ACCESS_SERCRET]

  @user = User.find_by( user_id: session[:user_id] )
  @user = User.create( user_id: session[:user_id] ) if @user.nil?
  @user.user_id = session[:user_id]
  @user.access_token = session[:access_token]
  @user.access_secret = session[:access_secret]
  @user.screen_name = @user.rubytter(:user, user_id: session[:user_id]).screen_name
end

helpers do
  def login?; session[:user_id] != nil end

  def login!; redirect '/' unless login? end
end

get '/signed/erb-test' do
  if session[:user_id] == nil
    redirect '/'
  else

    @user_screen_name = @user.screen_name
    erb :index
  end
end

get '/' do
  @coffee_server = CoffeeFtdi.new
  @ftdi_status = @coffee_server.init_ftdi
  
  # TODO:get drive enable status
  @eject_status = ( 0 == @coffee_server.prove_cup_sensor & 4 )
  unless @eject_status
    @coffee_server.cd_close
    sleep 2
    @eject_status = ( 0 == @coffee_server.prove_cup_sensor & 4 )
  end
  
  erb :index
end

get '/oauth' do
  request_token = User.consumer.get_request_token(:oauth_callback => "http://#{AppConfig[:COFFEE_ADDRESS]}/callback")
  session[:request_token] = request_token.token
  session[:request_secret] = request_token.secret
  redirect request_token.authorize_url
end

get '/callback' do
  request_token = OAuth::RequestToken.new(
    User.consumer,
    session[:request_token],
    session[:request_secret]
  )

  access_token = request_token.get_access_token(
                                                {},
    :oauth_token => params[:oauth_token],
    :oauth_verifier => params[:oauth_verifier]
  )

  session[:user_id] = access_token.params[:user_id]
  session[:access_token] = access_token.token
  session[:access_secret] = access_token.secret

  session.delete(:request_token)
  session.delete(:request_secret)

  u = User.find_by( user_id: session[:user_id] )
  u = User.create( user_id: session[:user_id] ) if u.nil?
  u.login_times += 1
  u.save!

  redirect '/'
end

get '/status' do
  # サーバ, Userのステータスを表示
  if session[:user_id] == nil
    "no login"
  else
    user = User.new
    user.user_id = session[:user_id]
    user.access_token = session[:access_token]
    user.access_secret = session[:access_secret]

    user.rubytter(:update, Time.now.to_i.inspect)
    Time.now.to_i.inspect
  end
end

get '/logout' do
  session.delete(:user_id)
  session.delete(:access_token)
  session.delete(:access_secret)

  redirect '/'
end

get '/signed/ctrl' do
  erb :ctrl
end

get '/signed/on' do
  @coffee_server = CoffeeFtdi.new
  @coffee_server.init_ftdi
  str = "@" + @user.rubytter(:user, user_id: session[:user_id]).screen_name + " がコーヒーを淹れています (" + Time.now.strftime("%Y/%m/%d %X") + ")"
  @server_twitter.rubytter(:update, str)
  @coffee_server.drip
  @status = "コーヒーを淹れています"
  erb :system_code
end

get '/off' do
  @coffee_server = CoffeeFtdi.new
  @coffee_server.init_ftdi
  @coffee_server.power_off
  @status = "off"
  erb :system_code
end

get '/signed/pour' do
  @coffee_server = CoffeeFtdi.new
  @coffee_server.init_ftdi
  @status = @coffee_server.prove_cup_sensor

  if ( 0 != @status & 8 ) && ( 0 == @status & 4 )
    if @coffee_server.pour_action(:pour, AppConfig[:COFFEE_SEC])
      @user.total += 1
      @user.save!

      @cup = Coffee_cup.create(:user_id => session[:user_id],
                               :screen_name => @user.screen_name )
      @cup.save!
      str = "@" + @user.screen_name + " がコーヒーを飲みました (" + Time.now.strftime("%Y/%m/%d %X") + ")"
      @server_twitter.rubytter(:update, str)

      @status = "poured! " + @status.to_s
    end
  else
    @status = "Ooops! Something wrong!!\n err" + @status.to_s
  end
  erb :system_code
end

get '/warm' do
  @coffee_server = CoffeeFtdi.new
  @coffee_server.init_ftdi
  @coffee_server.keep_warmin
  @status = "warm"
  erb :system_code
end

post '/service' do
  @coffee_server = CoffeeFtdi.new
  @ftdi_status = @coffee_server.init_ftdi
  @eject_status = ( 0 == @coffee_server.prove_cup_sensor & 4 )
  
  if "1" == params[:drip]
    @coffee_server.keep_warmin
  else
    if "1" == params[:cleaning]
      @coffee_server.pour_action(:pour, AppConfig[:WASH_SEC])
    else
      if "1" == params[:setup]
        @coffee_server.pour_action(:setup, 0)
      else
        if "1" == params[:eject]
          CoffeeFtdi.pour_sequence {
            @coffee_server.cd_open
            sleep 3
            @coffee_server.cd_close
          }
        else
          if "1" == params[:pump]
            CoffeeFtdi.pour_sequence do
              @coffee_server.pour
              sleep AppConfig[:COFFEE_SEC]
              @coffee_server.pour_off
            end
          else
            puts "blocking_status: " + @coffee_server.pour_action(:pour, AppConfig[:COFFEE_SEC]).to_s if "1" == params[:pour]
          end
        end
      end
    end
  end
  if "1" == params[:power_off]
    @coffee_server.power_off
  end
  erb :service
end

get '/service' do
  @coffee_server = CoffeeFtdi.new
  @ftdi_status = @coffee_server.init_ftdi
  @eject_status = ( 0 == @coffee_server.prove_cup_sensor & 4 )

  if "1" == params[:drip]
    @coffee_server.keep_warmin
  else
    @coffee_server.power_off if "1" == params[:power_off]
  end
  erb :service
end
