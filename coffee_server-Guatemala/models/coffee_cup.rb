require 'rubytter'
require 'oauth'
require "mongoid"

Mongoid.load!( 'config/mongoid.yml', :development )

class Coffee_cup
  include Mongoid::Document
  field :user_id
#  index { user_id: 1 }, { unique: true, background: true }
  field :screen_name

#  attr_accessible :user_id, :screen_name
  field :created_at, type: DateTime, default: lambda{Time.now} 
#  timestamps!
end
