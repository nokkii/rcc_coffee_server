require 'rubytter'
require 'oauth'
require 'mongoid'
require_relative 'config'

Mongoid.load!( 'config/mongoid.yml', :development )

class User
  include Mongoid::Document
  TOKEN_SECRET_KEYWORD = "guatemala"

  field :user_id
#  index { user_id: 1 }, { unique: true, background: true }
  
  field :screen_name
  field :login_times, type: Integer, default: 0
  field :total, type: Integer, default: 0
  field :access_token
  field :access_secret
  field :created_at, type: DateTime, default: lambda{Time.now}
  
#  timestamps!

#  attr_accessible :user_id, :screen_name, :access_token, :access_secret, :total, :login_times

  def self.consumer
    OAuth::Consumer.new(
      AppConfig[:CONSUMER_KEY],
      AppConfig[:CONSUMER_SECRET],
      site: "https://api.twitter.com"
    )
  end

  def token
    Digest::SHA1.hexdigest(self.id.to_s + TOKEN_SECRET_KEYWORD)
  end

  def rubytter(api, *args)
    @rubytter ||= OAuthRubytter.new(
      OAuth::AccessToken.new(
        User.consumer,
        access_token,
        access_secret
      )
    )
    begin
      @rubytter.method(api).call(*args)
    rescue Rubytter::APIError => error
      # Model.logger.warn "api error: #{error.message}"
      raise error
    end
  end
end
