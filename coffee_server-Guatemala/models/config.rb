require 'singleton'
require 'yaml'

class AppConfig
  include Singleton

  RESOURCE_FILE = "./config/app_config.yml"

  def self.[](key)
    self.instance[key]
  end

  def self.[]=(key, value)
    self.instance[key] = value
  end

  def [](key)
    load if @app_config == nil
    key = key.to_s if key.is_a?(Symbol)
    value = @app_config[key]    
    return value
  end

  def []=(key, value)
    load if @app_config == nil
    key = key.to_s if key.is_a?(Symbol)
    @app_config[key] = value
  end

  def load
    @app_config = YAML.load_file(RESOURCE_FILE)
  end

  def self.current_os
    return AppConfig[:OS_ENV]
  end

  def self.windows?
    return AppConfig[:OS_ENV] == 'win' ? true : false
  end

  def self.linux?
    return AppConfig[:OS_ENV] == 'linux' ? true : false
  end

  def self.other?
    return AppConfig[:OS_ENV] == 'other' ? true : false
  end
end
