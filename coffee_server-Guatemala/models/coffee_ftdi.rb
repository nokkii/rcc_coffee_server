require 'systemu'
require_relative "config"

#Unix compatible
require 'ftdi' unless AppConfig.windows?

class CoffeeFtdi
  def power_off
    @ctx.write_data [0x00]
  end

  def drip
    @ctx.write_data [0x01]
  end

  def keep_warmin
    @ctx.write_data [0x01]
  end

  def pour_off
    @ctx.write_data [0x01]
  end

  def pour_for_clean
    @ctx.write_data [0x02]
  end

  def pour
    @ctx.write_data [0x03]
  end

  def setup
    cd_open
    sleep 3
    10.times do
      @ctx.write_data [0x02]
      sleep 1
      @ctx.write_data [0x00]
    end
    cd_close
  end

  def prove_cup_sensor
    return @ctx.read_pins
  end

  def pour_action(api, seconds)
    self.pour_sequence do
      cd_open unless AppConfig.windows?
      AppConfig.windows? ? @status = `./bin/Input.exe` : @status = prove_cup_sensor

      sec = 0
      while ( 0 != @status & 4 ) || ( 0 == @status & 8 )
        sleep 0.2
        sec += 0.2
        break if sec > 8.0
        AppConfig.windows? ? @status = `./bin/Input.exe` : @status = prove_cup_sensor
        puts "a"
      end

      if ( 0 != @status & 8 ) && ( 0 != @status & 4 )
        AppConfig.windows? ? `./bin/Output.exe 3` : method(api).call
        sleep seconds
        AppConfig.windows? ? `./bin/Output.exe 1` : pour_off
        sleep 1
        puts "b"
      end
      cd_close unless AppConfig.windows?
    end
  end

  def init_ftdi
    @ctx = Ftdi::Context.new
    begin
      @ctx.usb_open_desc(0x0403, 0x6001,nil,AppConfig[:DEV_SERIAL])
      @ctx.set_bitmode(0x3f, :bitbang)
      sleep 0.2
      return true
    rescue Ftdi::Error => e
      @ctx = nil
      e.to_s
      return false
    end
  end

  def self.pour_sequence(&block)
    open(Dir.pwd + '/pour.lock', 'w') do |f|
      begin
        f.flock(File::LOCK_EX|File::LOCK_NB)
        yield
      ensure
        f.flock(File::LOCK_UN)
      end
    end
    true
  rescue Exception => ex
    false
  end

  def method_missing(action, *args, &block)
    self.class.__send__(action, *args, &block)
  end

  def cd_open( heat = false )
    AppConfig.windows? ? @status = `./bin/Input.exe` : @status = prove_cup_sensor
    if 0 == @status & 4
      heat = ( heat ? 1 : 0 )
      @ctx.write_data [ 0x20 | heat ]
      sleep 0.2
      @ctx.write_data [ 0x00 | heat ]
    end
  end

  def cd_close( heat = false )
    AppConfig.windows? ? @status = `./bin/Input.exe` : @status = prove_cup_sensor
    if 0 != @status & 4
      heat = ( heat ? 1 : 0 )
      @ctx.write_data [ 0x20 | heat ]
      sleep 0.2
      @ctx.write_data [ 0x00 | heat ]
    end
  end
end
